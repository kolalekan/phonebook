package utils;

import model.Contact;

import java.util.Date;
import java.util.Random;

public class AppUtils {
    public static String randomString(int length) {
        String alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return buildRandomCharacters(alphabets, length);
    }

    public static String randomNumbers(int length) {
        String numbers = "1234568790";
        return buildRandomCharacters(numbers, length);
    }

    private static String buildRandomCharacters(String alphabets, int length) {
        Random r = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int randomIndex = r.nextInt(alphabets.length());
            stringBuilder.append(alphabets.charAt(randomIndex));
        }

        return stringBuilder.toString();
    }

    public static Contact generateRandomContact() {
        return Contact.builder()
                .name(randomString(8))
                .phoneNumber(randomNumbers(11))
                .email(randomString(15))
                .date(new Date())
                .build();
    }



}
