package service.impl;

import exception.NoSuchContactException;
import exception.PhoneNumberExistsException;
import model.Contact;
import service.PhoneBookService;

import java.util.*;

public class PhoneBookServiceImpl implements PhoneBookService {

    private final PriorityQueue<Contact> heap = new PriorityQueue<>(Comparator.comparing(Contact::getName));
    private final Map<String, Contact> map = new HashMap<>();

    @Override
    public void addToContact(Contact contact) throws PhoneNumberExistsException {
        if (map.containsKey(contact.getPhoneNumber()))
            throw new PhoneNumberExistsException("Phone number exists already");
        this.heap.offer(contact);
        map.put(contact.getPhoneNumber(), contact);
    }

    @Override
    public Contact getUniqueContact(String phoneNumber) throws NoSuchContactException {
        if (!map.containsKey(phoneNumber)) {
            throw new NoSuchContactException("Phone number does not exist in the directory");
        }
        return map.get(phoneNumber);
    }

    @Override
    public List<Contact> getContacts() {
        List<Contact> contactList = new ArrayList<>();

        while (!heap.isEmpty()) {
            contactList.add(heap.poll());
        }
        return contactList;
    }
}
