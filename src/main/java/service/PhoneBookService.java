package service;

import exception.NoSuchContactException;
import exception.PhoneNumberExistsException;
import model.Contact;

import java.util.List;

public interface PhoneBookService {
    /**
     * Adds a contact to the phonebook
     *
     * @param  contact  a contact object that defines the contact to be saved
     */
    void addToContact(Contact contact) throws PhoneNumberExistsException;

    /**
     * Get a unique contact from the phonebook
     *
     * @param  phoneNumber the phone number needed to fetch a contact in the phonebook
     * @return a single contact based on the phone number supplied
     */
    Contact getUniqueContact(String phoneNumber) throws NoSuchContactException;

    /**
     * Get all the contacts in the phonebook
     *
     * @return the list of contacts
     */
    List<Contact> getContacts();
}
