package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
public class Contact {

    private String phoneNumber;
    private String name;
    private String email;
    private Date date;

}
