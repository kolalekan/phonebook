package exception;

public class PhoneNumberExistsException extends Exception {

    public PhoneNumberExistsException() {
    }

    public PhoneNumberExistsException(String message) {
        super(message);
    }
}
