import exception.NoSuchContactException;
import exception.PhoneNumberExistsException;
import model.Contact;
import service.PhoneBookService;
import service.impl.PhoneBookServiceImpl;
import utils.AppUtils;

public class PhoneBook {

    public static void main(String[] args) throws PhoneNumberExistsException, NoSuchContactException {
        PhoneBookService phoneBookService = new PhoneBookServiceImpl();

        phoneBookService.addToContact(Contact.builder()
                .name(AppUtils.randomString(8))
                .phoneNumber("090123")
                .build());
        phoneBookService.addToContact(AppUtils.generateRandomContact());
        phoneBookService.addToContact(AppUtils.generateRandomContact());

        phoneBookService.getUniqueContact("090123");

        phoneBookService.getContacts();

    }
}
