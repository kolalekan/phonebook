# PhoneBook

## Use of Heap and Map to store contact details
#### Maintain a heap for storing the contacts in order based on the name used to store the contact
#### Allows inserting into the heap in logarithm time. Contacts deletion is also in logarithmic time

#### Maintain map to store numbers, and their contacts in key-value pairs
#### Allows for easy access of contacts using their phone numbers in constant time